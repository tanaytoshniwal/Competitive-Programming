class Graph:

    def __init__(self, vertices):
        self.vertices = vertices
        self.graph = []
    
    def add_edge(src, wt, dest):
        self.graph.append({
            source: src,
            weight: wt,
            destination: dest
        });
    
    def connect()